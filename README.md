[![Latest Version on Packagist](https://img.shields.io/packagist/v/RoobieBoobieee/laravel-notifications-microsoft-teams.svg?style=flat-square)](https://packagist.org/packages/RoobieBoobieee/laravel-notifications-microsoft-teams)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Total Downloads](https://img.shields.io/packagist/dt/RoobieBoobieee/laravel-notifications-microsoft-teams.svg?style=flat-square)](https://packagist.org/packages/RoobieBoobieee/laravel-notifications-microsoft-teams)

This package makes it easy to send notifications using [Teams](https://teams.microsoft.com) with Laravel 5.5+ and 6.0

## Contents

- [Installation](#installation)
- [Usage](#usage)
- [Changelog](#changelog)
- [Security](#security)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)


## Installation

You can install the package via composer:

```bash
composer require roobieboobieee/laravel-notifications-microsoft-teams
```

Next, you must load the service provider:

```php
// config/app.php
'providers' => [
    // ...
    RoobieBoobieee\Teams\TeamsServiceProvider::class,
],
```

In every model you wish to be notifiable via Teams, you must add routeNotificationForTeams method:
```php
// config/services.php
// ...
public function routeNotificationForTeams()
{
    return $this->webhook;
}
```

## Usage

Implement a `toTeams` method in your `Notification` that returns a `Message` object.

```php

public function toTeams($notifiable)
{
  $message = new Message('Example message');

  $section = new Section();
  $section->activityTitle('Example title');
  $message->add($section);

  return $message;
}

```

## ToDo
- Implement `potentialAction`

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Security

If you discover any security related issues, please email robvankeilegom@gmail.com instead of using the issue tracker.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [Rob Van Keilegom](https://github.com/:author_username)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
