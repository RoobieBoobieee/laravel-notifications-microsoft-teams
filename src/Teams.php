<?php

namespace RoobieBoobieee\Teams;

use Exception;
use Illuminate\Support\Arr;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use RoobieBoobieee\Teams\Exceptions\CouldNotSendNotification;
use RoobieBoobieee\Teams\Message;

class Teams
{
  /**
  * Teams API base URL.
  *
  * @var string
  */
  protected $baseUrl = 'https://teamsapp.com/api';

  /**
  * API HTTP client.
  *
  * @var \GuzzleHttp\Client
  */
  protected $httpClient;

  /**
  * @param \GuzzleHttp\Client $http
  */
  public function __construct(HttpClient $http)
  {
    $this->httpClient = $http;
  }

  /**
  * Send a message to a Teams channel.
  *
  * @param array $data
  *
  * @return boolean
  *
  * @throws \RoobieBoobieee\Teams\Exceptions\CouldNotSendNotification
  */
  public function send($webhook, Message $message)
  {
    try {
      $response = $this->httpClient->request('POST', $webhook, [
        'body' => json_encode($message),
      ]);
    } catch (RequestException $exception) {
      if ($response = $exception->getResponse()) {
        throw CouldNotSendNotification::serviceRespondedWithAnHttpError($response);
      }

      throw CouldNotSendNotification::serviceCommunicationError($exception);
    } catch (Exception $exception) {
      throw CouldNotSendNotification::serviceCommunicationError($exception);
    }

    $body = json_decode($response->getBody(), true);

    return $body;
  }
}
