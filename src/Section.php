<?php

namespace RoobieBoobieee\Teams;

use RoobieBoobieee\Teams\Fact;

class Section implements \JsonSerializable
{

  private $activityTitle;

  private $activitySubtitle;

  private $activityImage;

  private $facts = [];

  public function activityTitle(string $data = null)
  {
    if ($data === null) {
      return $this->activityTitle;
    }

    $this->activityTitle = $data;
  }

  public function activitySubtitle(string $data = null)
  {
    if ($data === null) {
      return $this->activitySubtitle;
    }

    $this->activitySubtitle = $data;
  }

  public function activityImage(string $data = null)
  {
    if ($data === null) {
      return $this->activityImage;
    }

    $this->activityImage = $data;
  }

  public function addFact(Fact $fact)
  {
    $this->facts[] = $fact;
  }

  public function jsonSerialize()
  {
    $out = [];

    if ($this->activityTitle) {
      $out['activityTitle'] = $this->activityTitle;
    }

    if ($this->activitySubtitle) {
      $out['activitySubtitle'] = $this->activitySubtitle;
    }

    if ($this->activityImage) {
      $out['activityImage'] = $this->activityImage;
    }
    $out['facts'] = $this->facts;

    return $out;
  }
}
